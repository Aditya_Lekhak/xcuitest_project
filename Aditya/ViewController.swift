//
//  ViewController.swift
//  Aditya
//
//  Created by qksysadmin on 15/02/23.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


    @IBAction func showMessage(sender: UIButton)
    {
        let alert = UIAlertController(title: "Welcome to Demo", message: "This is demo alert", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil ))
        
        present(alert, animated: true, completion: nil )
    }
    
}

