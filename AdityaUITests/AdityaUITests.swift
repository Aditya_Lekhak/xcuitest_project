//
//  AdityaUITests.swift
//  AdityaUITests
//
//  Created by qksysadmin on 24/02/23.
//

import XCTest

final class AdityaUITests: XCTestCase {
    
    
    override func setUpWithError() throws {
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDownWithError() throws {
        let app = XCUIApplication()
        app.terminate()
        
    }
    
    func testExampleone1() throws
    {
        //automated amazon app by launching the browser, exploring different products by searching products, checking sign up funtionality by entering test data, checking cart, buy again funtionality of the app with assert validation.
        
        
        //launches mamazon app using bundle identifier
        let app = XCUIApplication(bundleIdentifier: "com.amazon.AmazonIN")
        app.launch()
        
        //assertion on fresh label and click on fresh label
        let label = app.staticTexts["Fresh"]
        XCTAssert(label.exists)
        label.tap()
        
        //click on categories tab
        let EMI = app.staticTexts["Categories"]
        
        //used wait for text condition of 10 sec
        EMI.waitForExistence(timeout: 10);
        EMI.tap()
        
        //click on snack option and checking assertion
        let sna = app.staticTexts["Snacks"]
        XCTAssert(sna.exists)
        sna.tap()
        
        //searching for the product on amazon fresh
        let ser = app.searchFields["Search in Amazon Fresh"]
        XCTAssert(ser.exists)
        ser.tap()
        
        //searching rocks on amazon fresh
        ser.typeText("Rocks")
        
        app.buttons["search"].tap()
        
        //checking results after searching
        XCTAssertTrue(app.staticTexts["RESULTS"].exists)
        
        //selecting one product from the listed results
        let org = app.staticTexts["Salubrity Premium Himalayan Pink Rock Salt, 2 Kg [Sendha Namak]"]
        org.tap()
        
        //going back to home option
        let Home = app.staticTexts["Home"]
        Home.waitForExistence(timeout: 10);
        Home.tap()
    
        //checking funtionality of cart option
        let Cart = app.staticTexts["Cart"]
        Cart.tap()
        
        XCTAssertTrue(app.staticTexts["Sign up now"].exists)
        
        //checking sign up funtionality
        app.staticTexts["Sign up now"].tap()
        
        //entering first and last name
        let nam = app.textFields["First and last name"]
        nam.waitForExistence(timeout: 10);
        nam.tap()
        nam.typeText("Quality")
        
        app.buttons["Done"].tap()
        
        //entering mobile number
        let qual = app.textFields["Mobile number"]
        qual.tap()
        qual.typeText("9999900000")
        
        app.buttons["Done"].tap()
        
        XCTAssertTrue(app.staticTexts["Show password"].exists)
        
        
        let poss = app.buttons["Cancel"]
        poss.tap()
        
        //exploring buy again tab
        let Buy = app.staticTexts["Buy again"]
        Buy.waitForExistence(timeout: 10);
        Buy.tap()
        
        //checking for assertion
        let Welcome = app.staticTexts["Welcome"]
        Welcome.waitForExistence(timeout: 10);
        
        let Cancel = app.buttons["Cancel"]
        Cancel.tap()
        
        //terminating the application
        app.terminate()
        
    }
    
}
